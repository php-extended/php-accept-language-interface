<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-accept-language-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Countable;
use Iterator;
use Stringable;

/**
 * AcceptLanguageChainInterface class file.
 * 
 * This class represents the whole chain of accept-language items.
 * 
 * @author Anastaszor
 * @extends \Iterator<integer, AcceptLanguageItemInterface>
 */
interface AcceptLanguageChainInterface extends Countable, Iterator, Stringable
{
	
	/**
	 * Adds the given item at the end of this chain.
	 * 
	 * @param AcceptLanguageItemInterface $item
	 * @return AcceptLanguageChainInterface
	 */
	public function appendItem(AcceptLanguageItemInterface $item) : AcceptLanguageChainInterface;
	
	/**
	 * Adds the given item at the beginning of this chain.
	 * 
	 * @param AcceptLanguageItemInterface $item
	 * @return AcceptLanguageChainInterface
	 */
	public function prependItem(AcceptLanguageItemInterface $item) : AcceptLanguageChainInterface;
	
	/**
	 * Gets whether this chain is empty.
	 * 
	 * @return boolean
	 */
	public function isEmpty() : bool;
	
	/**
	 * Gets a value of this chain that is compatible to be put into an accept
	 * language http header.
	 * 
	 * @return string
	 */
	public function getHeaderValue() : string;
	
}
