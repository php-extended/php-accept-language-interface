<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-accept-language-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use PhpExtended\Locale\LocaleInterface;
use Stringable;

/**
 * AcceptLanguageItemInterface interface file.
 * 
 * This interface represents a single item into an accept-language chain.
 * 
 * @author Anastaszor
 */
interface AcceptLanguageItemInterface extends Stringable
{
	
	/**
	 * Gets the locale used by this item.
	 * 
	 * @return LocaleInterface
	 */
	public function getLocale() : LocaleInterface;
	
	/**
	 * Gets the q-value of this item.
	 * 
	 * @return float
	 */
	public function getQValue() : float;
	
	/**
	 * Gets the value of this language item, suitable to be put into an http
	 * header.
	 * 
	 * @return string
	 */
	public function getHeaderValue() : string;
	
}
