<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-accept-language-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use PhpExtended\Parser\ParserInterface;

/**
 * AcceptLanguageChainParserInterface interface file.
 * 
 * This class represents a parser for Accept-Language http header values.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\ParserInterface<AcceptLanguageChainInterface>
 */
interface AcceptLanguageChainParserInterface extends ParserInterface
{
	
	// nothing to add
	// php does not accepts covariant return types between interfaces
	
}
